/** FEMTOLOON REV 2 CODE Winter Quarter 2017 -- Aria Tedjarati, Iskender Kushan **/
/*
* Current code status: RF and VCXO are to do
*/

/*********************************************************************************/
// Libraries

#include <stdlib.h>
#include <stdio.h>
#include <EEPROM.h>
#include <SPI.h>
#include <GPS.h>
// Note: this example uses Richard Nash's GPS library for the Adafruit Ultimate GPS
// Code located here: https://github.com/rvnash/ultimate_gps_teensy3
#include <aprs.h>
#include <Timers.h>

/*
#include <RH_TCP.h>
#include <RH_Serial.h>
#include <RH_ASK.h>
#include <RHTcpProtocol.h>
#include <RHSPIDriver.h>
#include <RHSoftwareSPI.h>
#include <RHRouter.h>
#include <RHReliableDatagram.h>
#include <RHNRFSPIDriver.h>
#include <RHMesh.h>
#include <RHHardwareSPI.h>
#include <RHGenericSPI.h>
#include <RHGenericDriver.h>
#include <RHDatagram.h>
#include <RHCRC.h>
#include <RadioHead.h>
*/
#include <RH_RF24.h>
#include <SPI.h>

//I2C
#include <i2c_t3.h>

/*********************************************************************************/
// Declaration of Constants

const int ledPin = 4;
//const int ledPin = 7; //testing OOK -> GPIO0 as the key
const int buckPWM = 3;
const int BB_EN = 2;
const int GPS_EN = 8;
const int GPS_RESET = 6;
const int SI4063_GPIO_0 = 7;
//const int SI4063_GPIO_1 =      33;
const int SI4063_GPIO_2 = 32;
const int SI4063_IRQ = 5;
const int SI4063_CS = 15;
const int V1P8_EN = 16;
const int RF_EN = 17;
const int CURRENT_SENSE = A8;
const int SOLAR_SENSE = A7;
const int SPRCAP_SENSE = A6;
const int T3P3BB_SENSE = A9;
const int GPS_BAUD = 9600;
const int DEBUG_BAUD = 9600;
const unsigned int PWM_FREQ = 100000;

//Radiohead pin definitions
uint8_t si4463_ss = 15;
uint8_t si4463_irq = 5;
uint8_t si4463_sdn = 17;


/*********************************************************************************/
//defines

//Timers
#define LOOP_TIMER			0
#define PRINT_TIMER			1
#define LED_TIMER			2
#define LOOP_TIME_INTERVAL 50
#define PRINT_TIME_INTERVAL 250
#define LED_TIME_INTERVAL	1000

//ANSI terminal escape sequences
#define OFF         "\033[0m"
#define BOLD        "\033[1m"
#define REVERSE     "\033[7m"
//#define GOTOXY( x,y) "\033[x;yH"   // Esc[Line;ColumnH
#define CLS          "\033[2J"     // Esc[2J Clear entire screen

//I2C
#define VCXOADDRESS 0x65
#define VCXOADDRESS_R 0xCB
#define VCXOADDRESS_W 0xCA



//defines to restart Teensy
#define RESTART_ADDR       0xE000ED0C
#define RESTART_VAL		   0x5FA0004
#define READ_RESTART()     (*(volatile uint32_t *)RESTART_ADDR)
#define WRITE_RESTART(val) ((*(volatile uint32_t *)RESTART_ADDR) = (val))

/*********************************************************************************/
// Objects

#define hsGPS             Serial1
HardwareSerial &GPS_PORT = Serial1;
#define debugSerial       Serial2 //Serial2 for 2MHz operation
//TinyGPSPlus               tinygps;
byte           gps_set_sucess = 0;

RH_RF24 rf24(si4463_ss, si4463_irq, si4463_sdn);
GPS gps(&GPS_PORT, true);
//SPISettings si4463(1000000, MSBFIRST, SPI_MODE0);

/*********************************************************************************/
//Flags

bool LED_STATE = 0;
int BUCK_PWM_STATE = 0;
bool BB_EN_STATE = 0;
bool GPS_EN_STATE = 1;
bool GPS_RST_STATE = 1;
bool V1P8_EN_STATE = 0;
bool RF_EN_STATE = 0;
bool VCXO_STATE = 0;
bool GPS_FLIGHT_MODE = 0;

/*********************************************************************************/
// DAC
float phase = 0.0;
float twopi = 3.14159 * 2;

/*********************************************************************************/
// APRS definitions

// Set your callsign and SSID here. Common values for the SSID are
#define S_CALLSIGN      "KK6MIR"
#define S_CALLSIGN_ID   1   // 11 is usually for balloons
// Destination callsign: APRS (with SSID=0) is usually okay.
#define D_CALLSIGN      "APRS"
#define D_CALLSIGN_ID   0
// Symbol Table: '/' is primary table '\' is secondary table
#define SYMBOL_TABLE '/' 
// Primary Table Symbols: /O=balloon, /-=House, /v=Blue Van, />=Red Car
#define SYMBOL_CHAR 'v'

struct PathAddress addresses[] = {
	{ (char *)D_CALLSIGN, D_CALLSIGN_ID },  // Destination callsign
	{ (char *)S_CALLSIGN, S_CALLSIGN_ID },  // Source callsign
	{ (char *)NULL, 0 }, // Digi1 (first digi in the chain)
	{ (char *)NULL, 0 }  // Digi2 (second digi in the chain)
};

/*********************************************************************************/
// Function prototypes

char readSerial();
void incrementPWM();
void decrementPWM();
void sendUBX(uint8_t *MSG, uint8_t len);
boolean getUBX_ACK(uint8_t *MSG);
void setGPSFlightMode();
void InitFemtoloon(void);
void VCXOHandshake(void);

/*********************************************************************************/
// Module variables
float current, solar_v, sprcap_v, bb_v;
char flightmode_attempts = 0;
char command = 0;

/*********************************************************************************/
// Main Program

void setup() {
	debugSerial.begin(DEBUG_BAUD);
	hsGPS.begin(GPS_BAUD);
	debugSerial.print("\e[1;1H");
	debugSerial.print(CLS);
	delay(5000);

	//init femtoloon hardware
	InitFemtoloon();

	debugSerial.print(REVERSE);
	debugSerial.print("FEMTOLOON REV B, A. TEDJARATI, I. KUSHAN ");
	debugSerial.print(__DATE__);
	debugSerial.print(" ");
	debugSerial.println(__TIME__);
	//debugSerial.printf("PWM Frequency set to: %u Hz \n",PWM_FREQ);
	debugSerial.println("\r'C' TO INCREMENT PWM, 'X' TO DECREMENT PWM, 'G' - TOGGLE GPS, 'R'  - TOGGLE RF, 'L' - TOGGLE LED, 'I' - PROGRAM VCXO");
	debugSerial.println("'B' - TOGGLE BB, 'P' - TOGGLE 1.8V, 'E' - TOGGLE GPS RESET, 'F' - TOGGLE GPS FLIGHT MODE, 'T' - MASTER RESET, 'Q' - RF24 INIT");
	debugSerial.println("LATEST COMMAND: ");
	debugSerial.print(OFF);

	//initialize I2C
	Wire1.begin(I2C_MASTER, 0x00, I2C_PINS_29_30, I2C_PULLUP_EXT, I2C_RATE_400);

	//init SPI
	SPI.setSCK(13);       // SCK on pin 13
	SPI.setMOSI(11);      // MOSI on pin 11
	SPI.setMISO(12);      // MOSI on pin 11
	SPI.setDataMode(SPI_MODE0);
	// Setting clock speed to 8mhz, as 10 is the max for the rfm22
	SPI.setClockDivider(SPI_CLOCK_DIV2);
	SPI.begin();

	//GPS setup
	gps.startSerial(GPS_BAUD);
	gps.setSentencesToReceive(OUTPUT_RMC_GGA);

	//APRS Setup
	aprs_setup(50, // number of preamble flags to send
		ledPin, // Use PTT pin
		0, // ms to wait after PTT to transmit
		0, 0 // No VOX ton
	);

	//init timers
	TMRArd_InitTimer(LOOP_TIMER, LOOP_TIME_INTERVAL);
	TMRArd_InitTimer(PRINT_TIMER, PRINT_TIME_INTERVAL);
	TMRArd_InitTimer(LED_TIMER, LED_TIME_INTERVAL);
}

/*********************************************************************************/

void loop() {

	//run the loop
	if (TestLoopTimerExpired()) RespLoopTimerExpired();

	//blink the led
	if (TestLEDTimerExpired()) RespLEDTimerExpired();

	//read commands
	command = readSerial();


	switch (command) {
		
	case 'a': case 'A':
	{
		/*
		int current_milis = millis();
		//analogWriteResolution(12);
		while (1) {
			float val = sin(phase) * 2000.0 + 2050.0;
			analogWrite(A14, (int)val);
			phase = phase + 0.02;
			if (phase >= twopi) phase = 0;
			delayMicroseconds(1);
			if (millis() - current_milis > 10000) break;
		}
		
		//analogWriteFrequency(buckPWM, PWM_FREQ);
		*/
		debugSerial.print("\e[6;1H");
		debugSerial.print("\033[K");
		debugSerial.print("\r");
		debugSerial.print("Transmitting APRS ...");

		//parse gps
		
		if (gps.sentenceAvailable()) gps.parseSentence();
		if (gps.newValuesSinceDataRead()) {
		gps.dataRead();
		Serial.printf("Location: %f, %f altitude %f\n\r",
		gps.latitude, gps.longitude, gps.altitude);
		}
		
		broadcastLocation("Stanford SSI");
		break;
	}
		break;
	case 'c': case 'C':
		incrementPWM();
		break;
	case 'x': case 'X':
		decrementPWM();
		break;
	case 'g': case 'G':
		GPS_EN_STATE = !GPS_EN_STATE;
		break;
	case 'q': case 'Q':
		Si4463Handshake();
		break;
	case 'r': case 'R':
		RF_EN_STATE = !RF_EN_STATE;
		break;
	case 'l': case 'L':
		LED_STATE = !LED_STATE;
		break;
	case 'b': case 'B':
		BB_EN_STATE = !BB_EN_STATE;
		break;
	case 'p': case 'P':
		V1P8_EN_STATE = !V1P8_EN_STATE;
		break;
	case 'i': case 'I':
		VCXO_STATE = !VCXO_STATE;
		VCXOHandshake();
		break;
	case 'e': case 'E':
		GPS_RST_STATE = !GPS_RST_STATE;
		break;
	case 'f': case 'F':
		GPS_FLIGHT_MODE = !GPS_FLIGHT_MODE;
		if (GPS_FLIGHT_MODE == 1) setGPSFlightMode();
		break;
	case 't': case 'T':
		WRITE_RESTART(RESTART_VAL);
		break;
	default:
		//unrecognized command, do nothing
		break;
	}

	//pipe out gps commands if gps is enabled
	if (!GPS_EN_STATE) {
		if (hsGPS.available()) {
			while (hsGPS.available()) {
				char temp_gps_read = hsGPS.read();
				if (temp_gps_read == '$')
					debugSerial.print("\033[K");
				debugSerial.write(temp_gps_read);
			}
		}
	}

	//update the status on the screen
	if (TestPrintTimerExpired()) RespPrintTimerExpired();

}

//Si4463 functions

void VCXOHandshake(void) {
	Wire1.beginTransmission(VCXOADDRESS);
	Wire1.write(byte(0x81));
	Wire1.write(byte(0x08));
	Wire1.endTransmission();

	/* This code causes the VCXO output to be zero
	Wire1.write(VCXOADDRESS_W);            // sends instruction byte
	Wire1.write(0x01); //write to register 1
	Wire1.write(0x08); //set LVCMOS
	Wire1.endTransmission();
	*/
	char tempread = Wire1.read();
	debugSerial.print("\e[6;1H");
	debugSerial.print("\033[K");
	debugSerial.print("\r");
	debugSerial.print(tempread, BIN);
}

void Si4463Handshake(void) {
	debugSerial.print("\033[K");
	debugSerial.println("Si 4460 Hello World");
	rf24.init();
	//rf24.setModemConfig(rf24.FSK_Rb0_5Fd1);
	debugSerial.print("\033[K");
	debugSerial.println("Init Complete");

	//get part info: Will return 0x4063 for Si4063, and 0x4463 for Si4463
	uint8_t buf[8];
	if (!rf24.command(RH_RF24_CMD_PART_INFO, 0, 0, buf, sizeof(buf))) {
		debugSerial.print("\033[K");
		debugSerial.println("SPI ERROR");
	}
	else {
		debugSerial.print("\033[K");
		debugSerial.println("SPI OK");
	}
	uint16_t deviceType2 = (buf[1] << 8) | buf[2];
	debugSerial.print("\033[K");
	debugSerial.print("device type = ");
	debugSerial.println(deviceType2, HEX);

	if (!rf24.setFrequency(144.5)) {
		Serial.println("setFrequency failed");
	}
	else {
		Serial.println("Frequency set");
	}

	//rf24.setTxPower(0x10);

	//try setting OOK mode with GPIO0 as the key
	
	//digitalWrite(si4463_ss, LOW);

	//Configure GPIOs

	//  modem mode
	/*
	SPI.transfer(byte(0x11));
	SPI.transfer(byte(0x20));
	SPI.transfer(byte(0x01));
	SPI.transfer(byte(0x00));
	SPI.transfer(byte(0x89));
	
	delay(25);
	
	// gpio modes
	SPI.transfer(byte(0x13));
	SPI.transfer(byte(0x04));
	SPI.transfer(byte(0x00));
	SPI.transfer(byte(0x00));
	SPI.transfer(byte(0x00));
	SPI.transfer(byte(0x00));
	SPI.transfer(byte(0x00));
	SPI.transfer(byte(0x00));
	*/
	////uint8_t buf2;
	//buf2 = SPI.transfer(0x01);
	//debugSerial.println(buf2, HEX);

	// take the SS pin high to de-select the chip:
	//digitalWrite(si4463_ss, HIGH);
	

	
	rf24.setModemConfig(rf24.FSK_Rb0_5Fd1);
	//rf24.setModemConfig(rf24.GFSK_Rb0_5Fd1);
	//uint8_t OOK_GPIO[] = { 0x04,0x00,0x00, 0x00, 0x00, 0x00, 0x00 };
	//uint8_t OOK_MODE[] = { 0x20,0x01,0x00,0x89};
	
	//rf24.command(RH_RF24_CMD_GPIO_PIN_CFG, OOK_GPIO, sizeof(OOK_GPIO));
	//rf24.command(RH_RF24_CMD_SET_PROPERTY, OOK_MODE, sizeof(OOK_GPIO));
	

	rf24.setTxPower(0x4f); //0x00 to 0x4f. 0x10 is default

	// Transmit a message
	uint8_t data[] = "Hello World! TEST TEST TEST TEST TEST TEST TEEEEEST TXTXTXTXTXTXTXTXTXTXTTXTXTXTXTXTXTXTXTXTXTXTXTEST TEST TEST TEST TEST TEST TEEEEEST TXTXTXTXTXTXTXTXTXTXTTXTXTXTXTXTXTXTXTXTXTXTX";
	rf24.send(data, sizeof(data));
	rf24.waitPacketSent();
	


}

/*********************************************************************************/
//Timer Functions

//led timer

void RespLEDTimerExpired(void) {
	LED_STATE = !LED_STATE;
	digitalWrite(ledPin, LED_STATE);
	TMRArd_InitTimer(LED_TIMER, LED_TIME_INTERVAL);
}

unsigned char TestLEDTimerExpired(void) {
	return (unsigned char)(TMRArd_IsTimerExpired(LED_TIMER));
}

//print the current status
void RespPrintTimerExpired(void) {
	debugSerial.print("\e[5;1H");
	debugSerial.print("\033[K");
	debugSerial.print("\r");
	debugSerial.print(BOLD);
	//debugSerial.print("Curr (mA): "); debugSerial.printf("%05.2f", current);
	debugSerial.print("I (mA): "); debugSerial.print(current);
	debugSerial.print(", SOLAR (V): "); debugSerial.print(solar_v);
	debugSerial.print(", SUPERCAP (V): "); debugSerial.print(sprcap_v);
	debugSerial.print(", BBOOST (V): "); debugSerial.print(bb_v);
	//debugSerial.print(", PWM: " + (String)BUCK_PWM_STATE);
	debugSerial.printf(", PWM: %03d", BUCK_PWM_STATE);
	debugSerial.print(", GPS_EN: " + (String)GPS_EN_STATE);
	debugSerial.print(", RF_EN: " + (String)RF_EN_STATE);
	debugSerial.print(", LED: " + (String)LED_STATE);
	debugSerial.print(", BB_EN: " + (String)BB_EN_STATE);
	debugSerial.print(", V1P8_EN: " + (String)V1P8_EN_STATE);
	debugSerial.print(", GPS_RST: " + (String)GPS_RST_STATE);
	debugSerial.print(", GPS_FLT: " + (String)GPS_FLIGHT_MODE);
	debugSerial.print(", VCXO: " + (String)VCXO_STATE);
	debugSerial.println();
	debugSerial.print(OFF);

	TMRArd_InitTimer(PRINT_TIMER, PRINT_TIME_INTERVAL);
}

unsigned char TestPrintTimerExpired(void) {
	return (unsigned char)(TMRArd_IsTimerExpired(PRINT_TIMER));
}


void RespLoopTimerExpired(void) {
	float current1 = (float)analogRead(CURRENT_SENSE) * 3.3 / (float)pow(2, 12) / 100.0 / 0.75 * 1000.0; // mA of current flow
	float solar_v1 = (float)analogRead(SOLAR_SENSE) * 3.3 / (float)pow(2, 12) * (51.0 + 20.0) / 20.0; // Solar voltage in Volts
	float sprcap_v1 = (float)analogRead(SPRCAP_SENSE) * 3.3 / (float)pow(2, 12) * (51.0 + 20.0) / 20.0; // Supercap voltage in Volts
	float bb_v1 = (float)analogRead(T3P3BB_SENSE) * 3.3 / (float)pow(2, 12) * 2.0; // buckboost voltage in Volts
	if (current1 < 200) current = current1;
	if (solar_v1 < 12) solar_v = solar_v1;
	if (sprcap_v1 < 12) sprcap_v = sprcap_v1;
	if (bb_v1 < 12) bb_v = bb_v1;
	digitalWrite(ledPin, LED_STATE);
	analogWrite(buckPWM, BUCK_PWM_STATE);    // Turn on Half Bridge
	digitalWrite(BB_EN, BB_EN_STATE);     // Turn on Buck boost
	digitalWrite(GPS_EN, GPS_EN_STATE);     // Turn on GPS
	digitalWrite(GPS_RESET, GPS_RST_STATE);// Turn off GPS reset
	digitalWrite(V1P8_EN, V1P8_EN_STATE);
	digitalWrite(RF_EN, RF_EN_STATE);
	TMRArd_InitTimer(LOOP_TIMER, LOOP_TIME_INTERVAL);
}

unsigned char TestLoopTimerExpired(void) {
	return (unsigned char)(TMRArd_IsTimerExpired(LOOP_TIMER));
}

/*********************************************************************************/
//read characters from serial
char readSerial() {
	if (debugSerial.available()) {
		char temp_command = debugSerial.read();
		debugSerial.print("\e[4;1H");
		debugSerial.print("\r");
		debugSerial.print(REVERSE);
		debugSerial.print("LATEST COMMAND: ");
		debugSerial.print(temp_command);
		debugSerial.print(OFF);
		debugSerial.print("\e[6;1H");
		return temp_command;
	}
	return 0;
}

/*********************************************************************************/
//increase PWM duty cycle by 12
void incrementPWM() {
	if (BUCK_PWM_STATE >= 4095) {
		BUCK_PWM_STATE = 4095;
	}
	else {
		BUCK_PWM_STATE += 50;
		if (BUCK_PWM_STATE >= 4095) BUCK_PWM_STATE = 4095;
	}
}

/*********************************************************************************/
//decrease PWM duty cycle by 12
void decrementPWM() {
	if (BUCK_PWM_STATE <= 0) {
		BUCK_PWM_STATE = 0;
	}
	else {
		BUCK_PWM_STATE -= 50;
		if (BUCK_PWM_STATE <= 0) BUCK_PWM_STATE = 0;
	}
}

/*********************************************************************************/
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Send a byte array of UBX protocol to the GPS
void sendUBX(uint8_t *MSG, uint8_t len) {
	for (int i = 0; i<len; i++) {
		hsGPS.write(MSG[i]);
		debugSerial.print(MSG[i], HEX);
	}
	hsGPS.println();
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Calculate expected UBX ACK packet and parse UBX response from GPS
boolean getUBX_ACK(uint8_t *MSG) {
	uint8_t b;
	uint8_t ackByteID = 0;
	uint8_t ackPacket[10];
	unsigned long startTime = millis();
	debugSerial.print(" * Reading ACK response: ");

	// Construct the expected ACK packet    
	ackPacket[0] = 0xB5;	// header
	ackPacket[1] = 0x62;	// header
	ackPacket[2] = 0x05;	// class
	ackPacket[3] = 0x01;	// id
	ackPacket[4] = 0x02;	// length
	ackPacket[5] = 0x00;
	ackPacket[6] = MSG[2];	// ACK class
	ackPacket[7] = MSG[3];	// ACK id
	ackPacket[8] = 0;		// CK_A
	ackPacket[9] = 0;		// CK_B

							// Calculate the checksums
	for (uint8_t i = 2; i<8; i++) {
		ackPacket[8] = ackPacket[8] + ackPacket[i];
		ackPacket[9] = ackPacket[9] + ackPacket[8];
	}

	while (1) {

		// Test for success
		if (ackByteID > 9) {
			// All packets in order!
			debugSerial.println(" (SUCCESS!)");
			return true;
		}

		// Timeout if no valid response in 3 seconds
		if (millis() - startTime > 3000) {
			debugSerial.println(" (FAILED!)");
			flightmode_attempts++;
			return false;
		}

		// Make sure data is available to read
		if (hsGPS.available()) {
			b = hsGPS.read();

			// Check that bytes arrive in sequence as per expected ACK packet
			if (b == ackPacket[ackByteID]) {
				ackByteID++;
				debugSerial.print(b, HEX);
			}
			else {
				ackByteID = 0;	// Reset and look again, invalid order
			}

		}
	}
}

/*********************************************************************************/

void setGPSFlightMode() {
	debugSerial.print("\e[6;1H");
	debugSerial.println("Setting uBlox nav mode: ");
	uint8_t setNav[] = {
		0xB5, 0x62, 0x06, 0x24, 0x24, 0x00, 0xFF, 0xFF, 0x06, 0x03, 0x00, 0x00, 0x00, 0x00, 0x10, 0x27, 0x00, 0x00,
		0x05, 0x00, 0xFA, 0x00, 0xFA, 0x00, 0x64, 0x00, 0x2C, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x16, 0xDC };
	while (!gps_set_sucess) {
		sendUBX(setNav, sizeof(setNav) / sizeof(uint8_t));
		gps_set_sucess = getUBX_ACK(setNav);
		if (flightmode_attempts == 5) {
			debugSerial.println("Flight mode set failed ...");
			gps_set_sucess = 1;
			return;
		}
	}
	gps_set_sucess = 0;
	return;
}

/*********************************************************************************/
void InitFemtoloon() {
	// initialize the digital pin as an output.
	pinMode(ledPin, OUTPUT);
	pinMode(buckPWM, OUTPUT);
	pinMode(BB_EN, OUTPUT);
	pinMode(GPS_EN, OUTPUT);
	pinMode(GPS_RESET, OUTPUT);
	pinMode(V1P8_EN, OUTPUT);
	pinMode(RF_EN, OUTPUT);
	pinMode(CURRENT_SENSE, INPUT);
	pinMode(SOLAR_SENSE, INPUT);
	pinMode(SPRCAP_SENSE, INPUT);
	pinMode(T3P3BB_SENSE, INPUT);
	analogWriteFrequency(buckPWM, PWM_FREQ);
	analogReadResolution(12);
	digitalWrite(ledPin, LED_STATE);
	analogWrite(buckPWM, BUCK_PWM_STATE);    // Turn on Half Bridge
	digitalWrite(BB_EN, BB_EN_STATE);     // Turn on Buck boost
	digitalWrite(GPS_EN, GPS_EN_STATE);     // Turn on GPS
	digitalWrite(GPS_RESET, GPS_RST_STATE);// Turn off GPS reset
	digitalWrite(V1P8_EN, V1P8_EN_STATE);
	digitalWrite(RF_EN, RF_EN_STATE);
	//setGPSFlightMode();
	//analogReference(INTERNAL); //does not work
	analogWriteResolution(12);
}

//broadcast location via APRS
void broadcastLocation(const char *comment)
{
	// If above 5000 feet switch to a single hop path
	int nAddresses;
	if (gps.altitude > 1500) {
		// APRS recomendations for > 5000 feet is:
		// Path: WIDE2-1 is acceptable, but no path is preferred.
		nAddresses = 3;
		addresses[2].callsign = "WIDE2";
		addresses[2].ssid = 1;
	}
	else {
		// Below 1500 meters use a much more generous path (assuming a mobile station)
		// Path is "WIDE1-1,WIDE2-2"
		nAddresses = 4;
		addresses[2].callsign = "WIDE1";
		addresses[2].ssid = 1;
		addresses[3].callsign = "WIDE2";
		addresses[3].ssid = 2;
	}

	// For debugging print out the path
	debugSerial.print("APRS(");
	debugSerial.print(nAddresses);
	debugSerial.print("): ");
	for (int i = 0; i < nAddresses; i++) {
		debugSerial.print(addresses[i].callsign);
		debugSerial.print('-');
		debugSerial.print(addresses[i].ssid);
		if (i < nAddresses - 1)
			debugSerial.print(',');
	}
	debugSerial.print(' ');
	debugSerial.print(SYMBOL_TABLE);
	debugSerial.print(SYMBOL_CHAR);
	debugSerial.println();

	// Send the packet
	
	aprs_send(addresses, nAddresses
		, gps.day, gps.hour, gps.minute
		, gps.latitude, gps.longitude // degrees
		, gps.altitude // meters
		, gps.heading
		, gps.speed
		, SYMBOL_TABLE
		, SYMBOL_CHAR
		, comment);
}